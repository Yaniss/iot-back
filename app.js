const express = require('express')
const cors = require('cors');
const http = require('http');
const routes = require('./src/routes');
let bodyParser = require('body-parser');

const app = express();
const particle = require('./src/particle/auth');

app.use(cors());
app.use(cors());
app.all('/*', function(req, res, next) {
    res.set({
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Methods": "*",
        "Access-Control-Allow-Headers": "'Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token'",
    });
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "*");
    console.log('CORS done !')
    next();
});
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

particle.authentification();


let server = http.createServer(app)

app.use(express.json())

app.use('/', routes);

let port = process.env.PORT || 8080;

server.listen(port, () => {
    console.log(`API is running on port : ${port}`);
})
