const express = require('express');
const router = express.Router();
const Test = require('./modules/test/testRoute');

const Lights = require('./modules/features/lights');
const Alarm = require('./modules/features/alarm');
const Locker = require('./modules/features/locker');
const Location = require('./modules/features/location');
const Ringtone = require('./modules/features/ringtone');
const Google = require('./modules/features/google');

router.route('/').get(Test.testRoute);

router.route('/lights').post(Lights.lights);
router.route('/locker').post(Locker.locker);
router.route('/alarm').post(Alarm.alarm);
router.route('/location').get(Location.location);
router.route('/ringtone').get(Ringtone.ringtone);
router.route('/google').post(Google.google);

module.exports = router;