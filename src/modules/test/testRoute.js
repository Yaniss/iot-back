const express = require('express');
const router = express.Router();

module.exports = {
    testRoute: async(req, res) => {
        console.log("GET /")
        response = {};
        response.status = 'Success';
        response.code = 200;
        res.status(response.code).json(response);
    }
}