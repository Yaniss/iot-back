const express = require('express');
const router = express.Router();

module.exports = {
    location: async(req, res) => {
        console.log("GET /location")
        response = {};
        response.location = {};
        response.location.longitude = 16.3466966;
        response.location.latitude = 46.3064622;
        response.status = 'Success';
        response.code = 200;
        res.status(response.code).json(response);
    }
}