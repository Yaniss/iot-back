const express = require('express');
const router = express.Router();
const photon = require('../../particle/callFunctions');
require('dotenv').config();

module.exports = {
    lights: async(req, res) => {
        console.log("POST /lights")
        console.log(process.env.TOKEN);
        response = {};
        lightsStatus = req.body.lightsStatus;
        if (lightsStatus) {
            if (lightsStatus === 'on') {
                console.log("The bike's lights are turned on");
                await photon.callFunctions("onLed").then(() => {
                    response.message = "The bike's lights are turned on";
                    response.status = 'Success';
                    response.code = 200;
                }).catch((err) => {
                    response.message = err;
                    response.status = 'ERROR';
                    response.code = 500;
                })
            } else if (lightsStatus === 'off') {
                console.log("The bike's lights are turned off");
                await photon.callFunctions("offLed").then((res) => {
                    response.message = "The bike's lights are turned off";
                    response.status = 'Success';
                    response.code = 200;
                }).catch((err) => {
                    response.message = err;
                    response.status = 'ERROR';
                    response.code = 500;
                })
            } else {
                response.status = 'Failed';
                response.message = 'Bad status / Status unknown';
                response.code = 400;
            }
        } else {
            response.status = 'Failed';
            response.message = 'No status provided';
            response.code = 400;
        }
        response.status = 'Success';
        response.code = 200;
        res.status(response.code).json(response);
    }
}