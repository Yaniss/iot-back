const express = require('express');
const router = express.Router();
const photon = require('../../particle/callFunctions');

module.exports = {
    alarm: async(req, res) => {
        console.log("POST /alarm")
        response = {};
        alarmStatus = req.body.alarmStatus;
        await photon.callFunctions("ring").then(() => {
            console.log("The bike's alarm is on");
            response.message = "The bike's alarm is on";
            response.status = 'Success';
            response.code = 200;
        }).catch((err) => {
            response.message = err;
            response.status = 'ERROR';
            response.code = 500;
        })
        /*if (alarmStatus) {
            if (alarmStatus === 'on') {
                console.log("The bike's alarm is on");
                response.message = "The bike's alarm is on";
                response.status = 'Success';
                response.code = 200;
            } else if (alarmStatus === 'off') {
                console.log("The bike's alarm is off");
                response.message = "The bike's alarm is off";
                response.status = 'Success';
                response.code = 200;
            } else {
                response.status = 'Failed';
                response.message = 'Bad status / Status unknown';
                response.code = 400;
            }
        } else {
            response.status = 'Failed';
            response.message = 'No status provided';
            response.code = 400;
        }*/
        response.status = 'Success';
        response.code = 200;
        res.status(response.code).json(response);
    }
}