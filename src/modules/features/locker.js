const express = require('express');
const router = express.Router();
const photon = require('../../particle/callFunctions');

module.exports = {
    locker: async(req, res) => {
        console.log("POST /locker")
        response = {};
        lockStatus = req.body.lockStatus;
        if (lockStatus) {
            if (lockStatus === 'lock') {
                console.log("The bike is locked");
                await photon.callFunctions("lock").then(() => {
                    response.message = 'The bike is locked';
                    response.status = 'Success';
                    response.code = 200;
                }).catch((err) => {
                    response.message = err;
                    response.status = 'ERROR';
                    response.code = 500;
                })
            } else if (lockStatus === 'unlock') {
                console.log("The bike is unlocked");
                await photon.callFunctions("unlock").then(() => {
                    response.message = 'The bike is unlocked';
                    response.status = 'Success';
                    response.code = 200;
                }).catch((err) => {
                    response.message = err;
                    response.status = 'ERROR';
                    response.code = 500;
                })
            } else {
                response.status = 'Failed';
                response.message = 'Bad status / Status unknown';
                response.code = 400;
            }
        } else {
            response.status = 'Failed';
            response.message = 'No status provided';
            response.code = 400;
        }
        res.status(response.code).json(response);
    }
}