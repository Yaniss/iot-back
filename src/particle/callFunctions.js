const axios = require('axios');
require('dotenv').config();
let querystring = require('querystring');
let deviceId = "510030000451353532373238";
let accessToken = process.env.TOKEN;


module.exports = {
    callFunctions : (functionName) => axios.post(`https://api.particle.io/v1/devices/${deviceId}/${functionName}`,
                {arg:functionName},{
                headers: {
                    Authorization: "Bearer " + process.env.TOKEN,
                    'content-type': 'application/x-www-form-urlencoded'
                },
            })
}
